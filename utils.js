
function createResult(err,data){
    if(err){
        return createError(err)
    }else{
        return createSuccess(data)
    }
}

function createError(err){
    const result = {status: "error"}
    result.error = err;
    // console.log(result)
    return result;
}

function createSuccess(data){
    const result = {status: "success"}
    result.data = data;
    // console.log(result)
    // console.table(data)
    return result;
}

module.exports = {
    createResult,
    createError,
    createSuccess
}

