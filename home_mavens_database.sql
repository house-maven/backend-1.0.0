CREATE DATABASE HOMEMAVENS;

USE HOMEMAVENS;

CREATE TABLE admin_info (
    id INT NOT NULL AUTO_INCREMENT,
    emailId VARCHAR(100),
    password VARCHAR(150),
    PRIMARY KEY (id),
    UNIQUE (emailId)
);

INSERT INTO admin_info (id, emailId, password)
VALUES (1, 'vaibhav@gmail.com', 'vaibhav');

INSERT INTO admin_info (id, emailId, password)
VALUES (2, 'sanket@gmail.com', 'sanket');

INSERT INTO admin_info (id, emailId, password)
VALUES (3, 'sarang@gmail.com', 'sarang');

INSERT INTO admin_info (id, emailId, password)
VALUES (4, 'nehal@gmail.com', 'nehal');


CREATE TABLE property_details (
    propertyId INT NOT NULL AUTO_INCREMENT,
    buildingName VARCHAR(500),
    flatNo INT,
    bhk INT,
    builtUpArea INT,
    carpetArea INT,
    price INT,
    propertyType VARCHAR(30),
    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (propertyId)
);

INSERT INTO property_details (propertyId, buildingName, flatNo, bhk, builtUpArea, carpetArea, price, propertyType)
VALUES (1, 'krishna kriti niwas', 6, 1, 600, 750, 15000, 'RENT');

INSERT INTO property_details (propertyId, buildingName, flatNo, bhk, builtUpArea, carpetArea, price, propertyType)
VALUES (2, 'morya apartment', 4, 2, 700, 900, 4000000, 'SELL');

INSERT INTO property_details (propertyId, buildingName, flatNo, bhk, builtUpArea, carpetArea, price, propertyType)
VALUES (3, 'ace augusta', 1104, 2, 850, 1000, 6500000, 'SELL');

INSERT INTO property_details (propertyId, buildingName, flatNo, bhk, builtUpArea, carpetArea, price, propertyType)
VALUES (4, 'yashwin', 1605, 3, 1200, 1350, 25000, 'RENT');

INSERT INTO property_details (propertyId, buildingName, flatNo, bhk, builtUpArea, carpetArea, price, propertyType)
VALUES (5, 'lake town', 201, 1, 480, 590, 20000, 'RENT');



CREATE TABLE address (
    addressId INT NOT NULL AUTO_INCREMENT,
    propertyId INT,
    street VARCHAR(500),
    city VARCHAR(200),
    state VARCHAR(200),
    pincode VARCHAR(10),
    PRIMARY KEY (addressId),
    FOREIGN KEY (propertyId) REFERENCES property_details(propertyId)
    ON DELETE CASCADE
);

INSERT INTO address (addressId, propertyId, street, city, state, pincode)
VALUES (1, 1, 'ganesh talav', 'pune', 'MH', '411035');

INSERT INTO address (addressId, propertyId, street, city, state, pincode)
VALUES (2, 2, 'sukhsagar nagar', 'pune', 'MH', '411046');

INSERT INTO address (addressId, propertyId, street, city, state, pincode)
VALUES (3, 3, 'hinjewadi phase 2', 'pune', 'MH', '411057');

INSERT INTO address (addressId, propertyId, street, city, state, pincode)
VALUES (4, 4, 'hinjewadi phase 2', 'pune', 'MH', '411057');

INSERT INTO address (addressId, propertyId, street, city, state, pincode)
VALUES (5, 5, 'bibewadi road', 'pune', 'MH', '411007');




CREATE TABLE images (
    imageId INT NOT NULL AUTO_INCREMENT,
    propertyId INT,
    imageName VARCHAR(200),
    PRIMARY KEY (imageId),
    FOREIGN KEY (propertyId) REFERENCES property_details(propertyId)
    ON DELETE CASCADE
);

INSERT INTO images (imageId, propertyId, imageName)
VALUES (1, 1, 'image11');

INSERT INTO images (imageId, propertyId, imageName)
VALUES (2, 1, 'image12');

INSERT INTO images (imageId, propertyId, imageName)
VALUES (3, 2, 'image21');

INSERT INTO images (imageId, propertyId, imageName)
VALUES (4, 2, 'image22');

INSERT INTO images (imageId, propertyId, imageName)
VALUES (5, 3, 'image31');

INSERT INTO images (imageId, propertyId, imageName)
VALUES (6, 3, 'image32');

INSERT INTO images (imageId, propertyId, imageName)
VALUES (7, 3, 'image33');

INSERT INTO images (imageId, propertyId, imageName)
VALUES (8, 4, 'image41');

INSERT INTO images (imageId, propertyId, imageName)
VALUES (9, 4, 'image42');

INSERT INTO images (imageId, propertyId, imageName)
VALUES (10, 4, 'image43');

INSERT INTO images (imageId, propertyId, imageName)
VALUES (11, 5, 'image51');

INSERT INTO images (imageId, propertyId, imageName)
VALUES (12, 5, 'image52');


CREATE TABLE user_info (
    userId INT NOT NULL AUTO_INCREMENT,
    firstName VARCHAR(100),
    lastName VARCHAR(100),
    email VARCHAR(100) NOT NULL,
    phone VARCHAR(50),
    type VARCHAR(50),
    password VARCHAR(150),
    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    activationToken VARCHAR(500),
    isActive INT DEFAULT 0,
    PRIMARY KEY (userId),
    UNIQUE (email)
);

INSERT INTO user_info (userId, firstName, lastName, email, phone, type, password, activationToken, isActive)
VALUES ( 1, 'akshay', 'mankar', 'akshay@gmail.com', '9025624575', 'agent', '6e33209b8019450f2b2cf8922ffbe6ea37149e66be07577e8fe0bf0261891f94', 'sdvsuyukhjani5986', 1);

INSERT INTO user_info (userId, firstName, lastName, email, phone, type, password, activationToken, isActive)
VALUES ( 2, 'mandar', 'kulkarni', 'mandar@gmail.com', '9025624575', 'owner', '287aff41e1f6a8107fa18f5805992e8ac22da0a47427a95295cb96df0245c7ac', 'bhsdhahsyjdahsdhhj', 1);

INSERT INTO user_info (userId, firstName, lastName, email, phone, type, password, activationToken, isActive)
VALUES ( 3, 'prashant', 'patil', 'prashant@gmail.com', '9025624575', 'agent', 'f173b46b650d9708d31b642021d04ed380c54db71fc9f0ab829d1c2597d37f3d', '486529asfczckjmc', 1);

INSERT INTO user_info (userId, firstName, lastName, email, phone, type, password, activationToken, isActive)
VALUES ( 4, 'amit', 'jha', 'amit@gmail.com', '9025624575', 'owner', '623f0b5584eb86d9f905e52679a9ce3bca0bd91a950a03e0eaa1b2f8bb3e9908', 'budshfsialk165das', 1);

INSERT INTO user_info (userId, firstName, lastName, email, phone, type, password, activationToken, isActive)
VALUES ( 5, 'sanket', 'chidrewar', 'sanket@gmail.com', '9025624575', 'customer', 'a9a5067ae9df2964fb64b0bf88537b8d2f469cd30ad747b0844fb23746458bc3', 'hjashdiakdnaskjd', 1);

INSERT INTO user_info (userId, firstName, lastName, email, phone, type, password, activationToken, isActive)
VALUES ( 6, 'vaibhav', 'gholap', 'vaibhav@gmail.com', '9025624575', 'customer', 'e444bd3bca2736f39ca65d53853891a6b0f553b8def766531fbbd6d1b8800521', 'hdsbfgsukdjhshds', 1);

INSERT INTO user_info (userId, firstName, lastName, email, phone, type, password, activationToken, isActive)
VALUES ( 7, 'sarang', 'bodhe', 'sarang@gmail.com', '9025624575', 'customer', 'cee0be1b5137cbee3bff832983426b82d379046bf8eedcdea62b3b066d587616', 'shdgkasudjahsdg', 1);


CREATE TABLE revenue (
    id INT NOT NULL AUTO_INCREMENT,
    propertyId INT,
    userId INT,
    amount INT,
    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (userId) REFERENCES user_info(userId)  
    ON DELETE CASCADE,
    FOREIGN KEY (propertyId) REFERENCES property_details(propertyId)
    ON DELETE CASCADE
);

INSERT INTO revenue (id, propertyId, userId, amount)
VALUES (1, 1, 1, 150);

INSERT INTO revenue (id, propertyId, userId, amount)
VALUES (2, 2, 1, 0);

INSERT INTO revenue (id, propertyId, userId, amount)
VALUES (3, 3, 2, 150);

INSERT INTO revenue (id, propertyId, userId, amount)
VALUES (4, 4, 3, 150);

INSERT INTO revenue (id, propertyId, userId, amount)
VALUES (5, 5, 4, 0);


CREATE TABLE property_requirement (
    id INT NOT NULL AUTO_INCREMENT,
    userId INT,
    propertyId INT,
    PRIMARY KEY (id),
    FOREIGN KEY (userId) REFERENCES user_info(userId),
    FOREIGN KEY (propertyId) REFERENCES property_details(propertyId)
    ON DELETE CASCADE
);

INSERT INTO property_requirement (id, userId, propertyId)
VALUES (1, 7, 2);


CREATE TABLE properties (
    id INT NOT NULL AUTO_INCREMENT,
    userId INT,
    propertyId INT,
    isActive INT DEFAULT 0,
    PRIMARY KEY (id),
    FOREIGN KEY (userId) REFERENCES user_info(userId),
    FOREIGN KEY (propertyId) REFERENCES property_details(propertyId)
    ON DELETE CASCADE
);

INSERT INTO properties (id, userId, propertyId, isActive)
VALUES (1, 1, 1, 1);

INSERT INTO properties (id, userId, propertyId, isActive)
VALUES (2, 1, 2, 0);

INSERT INTO properties (id, userId, propertyId, isActive)
VALUES (3, 2, 3, 1);

INSERT INTO properties (id, userId, propertyId, isActive)
VALUES (4, 3, 4, 1);

INSERT INTO properties (id, userId, propertyId, isActive)
VALUES (5, 4, 5, 0);



CREATE TABLE ratings_and_feedback (
    id INT NOT NULL AUTO_INCREMENT,
    propertyId INT,
    userId INT,
    feedback VARCHAR(200),
    ratings INT,
    createdOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (userId) REFERENCES user_info(userId)
    ON DELETE CASCADE,
    FOREIGN KEY (propertyId) REFERENCES property_details(propertyId)
    ON DELETE CASCADE
);

INSERT INTO ratings_and_feedback (id, propertyId, userId, feedback, ratings)
VALUES (1, 1, 5, 'Good Service', 4);

INSERT INTO ratings_and_feedback (id, propertyId, userId, feedback, ratings)
VALUES (2, 3, 6, 'Nice agent', 5);

INSERT INTO ratings_and_feedback (id, propertyId, userId, feedback, ratings)
VALUES (3, 4, 7, 'Bad Service', 2);

DELIMITER /
CREATE TRIGGER tg_revenue AFTER INSERT ON properties FOR EACH ROW
BEGIN
INSERT INTO revenue(propertyId,userId,amount)VALUES(NEW.propertyId,NEW.userId,100);
END/
DELIMITER ;






















