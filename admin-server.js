//importing required node packages 
const express = require('express');
const { request, Router } = require('express');
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const cors = require('cors')

//importing different routes written in routes folder
const adminRoutes = require('./admin/routes/admin')
const adminProperties = require('./admin/routes/properties')
const adminPropertyRequirement = require('./admin/routes/propertyRequirement')
const adminRatingAndFeedback = require('./admin/routes/ratingAndFeedback')
const adminRevenue = require('./admin/routes/revenue')
const adminUser = require('./admin/routes/user')

//creating express server
const app = express();

app.use(cors())

app.use(bodyParser.json())
// app.use(express.json())

// add a middleware for getting the id from token
function getUserId(request, response, next){
    if(request.url == '/signin' || request.url == '/signup'){
        next();
    }else{
        try {
            const token = request.headers['token']
            // console.log(token)
            const data = jwt.verify(token, '0123456789')
            // console.log(`INFO : ${data}`)
            // console.log(data['id'])
            request.userId = data['id']
            next();
        } catch (error) {
            response.status(401)
            response.send({status: 'error', error: 'protected api'})
        }
    }
}

//middleware for authenticating user by generating id back from token
 app.use(getUserId)

//middleware code for calling different routes
app.use('/',adminRoutes)
app.use('/properties',adminProperties)
app.use('/property_requirement',adminPropertyRequirement)
app.use('/rating_and_feedback',adminRatingAndFeedback)
app.use('/revenue',adminRevenue)
app.use('/user',adminUser)


//admin server test request
app.get('/',(request, response) => {
    response.send('hello from admin server to postmon');
})


//listening to server on port 3000
app.listen(3000,'0.0.0.0', () => {
    console.log('server started on port 3000')
})