const express = require('express');
const { response, request } = require('express');
const jwt = require('jsonwebtoken')

const connection = require('../../database_connection')
const utils = require('../../utils')


const router = express.Router();

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------GET----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.get('/',(request, response) => {
    // response.send('hello from get admin.js')
    const statement = `SELECT * FROM admin_info;`;

    connection.query(statement, (err, data) => {
        // response.send('hello')
        // console.log(utils.createResult(err,data))
        response.send(utils.createResult(err,data))
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------POST----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.post('/signin',(request,response) => {

    // console.log(request.body['emailId'])
    // console.log(request.body['password'])

    const {emailId, password} = request.body

    // console.log(emailId)
    // console.log(password)

    //ToDo : using SHA256() encrypt the password and then compare with database password
    const statement = `SELECT * FROM admin_info WHERE emailId = '${emailId}' AND password = '${password}'`

    connection.query(statement, (err, data) => {
        // response.send(utils.createResult(err, data))

        if(err){
        response.send(utils.createError(err))
        }else{
            if(data.length == 1){
                var token = jwt.sign({ id: data[0]['id'] }, '0123456789');
                data[0].token = token;
                data[0].password = undefined;
                data[0].id = undefined;
                response.send(utils.createSuccess(data))
            }else{
                response.send('Entered Email id and password is invalid')
            }
        }
    })

    // response.send('hello from post signin admin.js')
    
})

router.post('/signup',(request,response) => {
    const {email, password} = request.body
    // response.send('hello from post signup admin.js')

    const statement = `INSERT INTO admin_info(emailId, password) VALUES('${email}', '${password}');`

    connection.query(statement, (err, data) => {
        response.send(utils.createResult(err, data))
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------PUT----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.put('/',(request,response) => {
    response.send('hello from put admin.js')
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------DELETE----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.delete('/',(request,response) => {
    response.send('hello from delete admin.js')
})

// --------------------------------------------------------------------------------------------------------

module.exports = router;