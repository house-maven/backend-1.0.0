const express = require('express');
const { response, request } = require('express');

// * Creating "connection" object for mysql connection
const connection = require("../../database_connection")

// * Importing utility object to generate result
const utils = require('../../utils')

const router = express.Router();

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------GET----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.get('/', (request, response) => {
    const queryString = "select SUM(amount) as amount, MONTHNAME(createdOn) as month from revenue Group By(MONTHNAME(createdOn));"

    connection.query(queryString, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        }
        else {
            response.send(utils.createSuccess(data))
        }
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------POST----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.post('/', (request, response) => {
    response.send('hello from post revenue.js')
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------PUT----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.put('/', (request, response) => {
    response.send('hello from put revenue.js')
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------DELETE----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.delete('/', (request, response) => {
    response.send('hello from delete revenue.js')
})

// --------------------------------------------------------------------------------------------------------

module.exports = router;