const express = require('express');
const { response, request } = require('express');

// * Creating "connection" object for mysql connection
const connection = require("../../database_connection")

// * Importing utility object to generate result
const utils = require('../../utils')

const router = express.Router();

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------GET----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.get('/', (request, response) => {
    const queryString = `SELECT pr.id,u.userId,u.firstName,u.lastName,u.email,u.phone,u.type,p.propertyId,p.bhk,p.price,a.addressId,a.street,a.city,a.state,a.pincode FROM 
    property_requirement pr INNER JOIN user_info u ON pr.userId=u.userId 
    INNER JOIN property_details p ON pr.propertyId=p.propertyId 
    INNER JOIN address a ON a.propertyId=p.propertyId;` 

    connection.query(queryString, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        }
        else {
            response.send(utils.createSuccess(data))
        }
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------POST----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.post('/', (request, response) => {
    response.send('hello from post propertyRequirement.js')
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------PUT----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.put('/', (request, response) => {
    response.send('hello from put propertyRequirement.js')
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------DELETE----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.delete('/', (request, response) => {
    response.send('hello from delete propertyRequirement.js')
})

// --------------------------------------------------------------------------------------------------------

module.exports = router;