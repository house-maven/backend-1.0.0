const express = require('express');
const { response, request } = require('express');
const connection = require('../../database_connection')
const utils = require('../../utils');
const { createSuccess } = require('../../utils');
const config = require('../../config');


const router = express.Router();

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------GET----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.get('/',(request, response) => {
    // response.send('hello from get properties.js')
    const statement = `SELECT p.id, p.isActive, u.userId, u.firstName, u.lastName, u.email, u.phone, u.type, pd.propertyId, pd.buildingName, pd.flatNo, pd.bhk, pd.builtUpArea, pd.carpetArea, pd.price, pd.propertyType, a.addressId, a.street, a.city, a.state, a.pincode
                        FROM properties p INNER JOIN user_info u ON p.userId = u.userId
                        INNER JOIN property_details pd ON p.propertyId = pd.propertyId
                        INNER JOIN address a ON pd.propertyId = a.propertyId`
    
    connection.query(statement, (err, data) => {
        if(err){
            response.send(utils.createError(err))
        }else{
            var property = []
            for(let index =0; index < data.length; index++)
            {
                const prop_length = data.length
                var count = 0;
                const property_info = data[index]
                property_info.images = []

                const images_statement = `SELECT imageName from images where propertyId = ${property_info['propertyId']}`
                connection.query(images_statement, (err, data) => {
                    if(err){
                        response.send(utils.createError(err))
                    }else{
                        data.forEach(element => {
                            // property_info.images.push(element['imageName'])
                            property_info.images.push(`${config.host}:4000/properties/get-image/${element['imageName']}`)

                        });
                    
                    const rf_statement = `SELECT r.feedback,r.ratings,r.userId,u.firstName,u.lastName 
                                        FROM ratings_and_feedback r INNER JOIN user_info u ON r.userId = u.userId 
                                        WHERE propertyId= ${property_info.propertyId}; `
                        connection.query(rf_statement, (err, data) => {
                            if(err){
                                response.send(utils.createError(err))
                            }else{
                                property_info.feedback = []
                                var total_rating = 0
                                var no_of_ratings = 0
                                data.forEach(element => {
                                    no_of_ratings++;
                                    const feedback_user_and_feedback = {}
                                    feedback_user_and_feedback.userId = element['userId']
                                    feedback_user_and_feedback.firstName = element['firstName']
                                    feedback_user_and_feedback.lastName = element['lastName']
                                    feedback_user_and_feedback.feedback = element['feedback']
                                    property_info.feedback.push(feedback_user_and_feedback)
                                    total_rating += element['ratings']
                                });
                                property_info.avg_rating = parseFloat(total_rating/no_of_ratings)
                                
                                      property.push(property_info)  
                                      if(++count == prop_length)
                                      {
                                        response.send(createSuccess(property))
                                      }
                            }
                        })
                    }
                })
            }
        }
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------POST----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.post('/',(request,response) => {
    response.send('hello from post properties.js')
})

router.post('/:status',(request,response) => {
    console.log(request.params['status'])
    response.send('hello from post change property status properties.js')
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------PUT----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.put('/:propertiesId/:isActive',(request,response) => {
    // response.send('hello from put properties.js')
    const { propertiesId, isActive } = request.params
    const statement = `update properties SET isActive = ${isActive} where id = ${propertiesId};`
    connection.query(statement, (err, data) => {
        response.send(utils.createResult(err, data))
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------DELETE----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.delete('/:propertyId',(request,response) => {
    // response.send('hello from delete properties.js')
    const { propertyId } = request.params
    // console.log(propertyId)
    const statement = `delete from property_details where propertyId = ${propertyId} `

    connection.query(statement, (err, data) => {
        response.send(utils.createResult(err,data))
    })
})

// --------------------------------------------------------------------------------------------------------

module.exports = router;