const express = require('express');
const { response, request } = require('express');

// * Creating "connection" object for mysql connection
const connection = require("../../database_connection")

// * Importing utility object to generate result
const utils = require('../../utils')

const router = express.Router();

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------GET----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.get('/', (request, response) => {
    const queryString = "SELECT userId,firstName,lastName,email,phone,type,isActive,createdOn FROM user_info"

    connection.query(queryString, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        }
        else {
            response.send(utils.createSuccess(data))
        }
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------POST----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.post('/:status', (request, response) => {
    console.log(request.params['status'])
    response.send('hello from post user.js')
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------PUT----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.put('/:userId/:isActive', (request, response) => {
    // response.send('hello from put user.js')
    const { userId, isActive } = request.params
    const statement = `update user_info SET isActive = ${isActive} where userId = ${userId};`
    connection.query(statement, (err, data) => {
        response.send(utils.createResult(err, data))
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------DELETE----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.delete('/', (request, response) => {
    response.send('hello from delete user.js')
})

// --------------------------------------------------------------------------------------------------------

module.exports = router;