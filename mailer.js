const mailer = require('nodemailer');
const config = require('./config');
function SendMail(email,body,subject,callback)
{
    let transporter = mailer.createTransport({
        service : "gmail",
        auth : {
            user : config.email,
            pass : config.password
        }
    })

    transporter.sendMail({
        from : config.email,
        to : email,
        subject : subject,
        html : body
    },callback)
}

module.exports = SendMail