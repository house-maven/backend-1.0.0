const express = require('express');
const { response, request } = require('express');
const jwt = require('jsonwebtoken')
const connection = require('../../database_connection')
const utils = require('../../utils');
const fs = require('fs');

//multer : for uploading images
var multer = require('multer');
const config = require('../../config');
const upload = multer({ dest : 'images/'})
const router = express.Router();

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------GET----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.get('/',(request, response) => {
    // response.send('hello from get properties.js')
     // response.send('hello from get properties.js')

     console.log("Hello")

     const statement = `SELECT p.id, p.isActive, u.userId, u.firstName, u.lastName, u.email, u.phone, u.type, pd.propertyId, pd.buildingName, pd.flatNo, pd.bhk, pd.builtUpArea, pd.carpetArea, pd.price, pd.propertyType, a.addressId, a.street, a.city, a.state, a.pincode
     FROM properties p INNER JOIN user_info u ON p.userId = u.userId
     INNER JOIN property_details pd ON p.propertyId = pd.propertyId
     INNER JOIN address a ON pd.propertyId = a.propertyId
     where u.userId = ${request['userId']}`

    //  console.log(statement);

connection.query(statement, (err, data) => {
if(err){
response.send(utils.createError(err))
}else{
if(data.length == 0 )
{
    response.send(utils.createError("no any record found"))
}
var property = []
for(let index =0; index < data.length; index++)
{
const prop_length = data.length
var count = 0;
const property_info = data[index]
property_info.images = []

const images_statement = `SELECT imageName from images where propertyId = ${property_info['propertyId']}`
connection.query(images_statement, (err, data) => {
 if(err){
     response.send(utils.createError(err))
 }else{
     data.forEach(element => {
         property_info.images.push(`${config.host}:4000/properties/get-image/${element['imageName']}`)
     });
 
 const rf_statement = `SELECT r.feedback,r.ratings,r.userId,u.firstName,u.lastName 
                     FROM ratings_and_feedback r INNER JOIN user_info u ON r.userId = u.userId 
                     WHERE propertyId= ${property_info.propertyId}; `
     connection.query(rf_statement, (err, data) => {
         if(err){
             response.send(utils.createError(err))
         }else{
             property_info.feedback = []
             var total_rating = 0
             var no_of_ratings = 0
             data.forEach(element => {
                 no_of_ratings++;
                 const feedback_user_and_feedback = {}
                 feedback_user_and_feedback.userId = element['userId']
                 feedback_user_and_feedback.firstName = element['firstName']
                 feedback_user_and_feedback.lastName = element['lastName']
                 feedback_user_and_feedback.feedback = element['feedback']
                 property_info.feedback.push(feedback_user_and_feedback)
                 total_rating += element['ratings']
             });
             property_info.avg_rating = parseFloat(total_rating/no_of_ratings)
             
                   property.push(property_info)  
                   if(++count == prop_length)
                   {
                     response.send(utils.createSuccess(property))
                   }
         }
     })
 }
})
}
}
})

})

//get list of particular property details by it's propertyID
router.get('/:propertyId',(request, response) => {
    
    const {propertyId} = request.params;

     const statement = `SELECT p.id, p.isActive, u.userId, u.firstName, u.lastName, u.email, u.phone, u.type, pd.propertyId, pd.buildingName, pd.flatNo, pd.bhk, pd.builtUpArea, pd.carpetArea, pd.price, pd.propertyType, a.addressId, a.street, a.city, a.state, a.pincode
     FROM properties p INNER JOIN user_info u ON p.userId = u.userId
     INNER JOIN property_details pd ON p.propertyId = pd.propertyId
     INNER JOIN address a ON pd.propertyId = a.propertyId
     where u.userId = ${request['userId']} AND p.propertyId = ${propertyId}`

    //  console.log(statement);

    connection.query(statement, (err, data) => {
        if(err){
            response.send(utils.createError(err))
        }else{
            if(data.length == 0 )
            {
                response.send(utils.createError("no any record found"))
            }else{
                response.send(utils.createSuccess(data));
            }
        }
    })
})

router.get('/get-image/:image',(request, response) => {
 
    const {image} = request.params
    const file = fs.readFileSync(`${__dirname}/../../images/${image}`)
    // console.log(image)
    // console.log(`${__dirname}/../../images/${image}`)
  
    response.send(file)
  
  })

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------POST----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------
router.post('/upload-image/:propertyId', upload.single('images'), (request, response) => {
    const {propertyId} = request.params

        const fileName = request.file.filename;
        const statement = `insert into images(imageName, propertyId)
                            VALUES('${fileName}',${propertyId})`
        console.log(statement);
        connection.query(statement, (error, data) => {
            if(error)
            {
                response.send(utils.createError(error))
            }else{
                response.send(utils.createSuccess("Images Uploaded Successfully"))
            }
        })
  

})
router.post('/',upload.array('images',3),(request,response) => {
    // response.send('hello from post properties.js')
    const {buildingName, flatNo, bhk, builtUpArea, carpetArea, price, propertyType, street, city, state, pincode, images  } = request.body;
    var addressId, propertyId, imageId, fileName;
    // console.log(buildingName+" "+bhk+" "+builtUpArea+" "+carpetArea+" "+price+" "+propertyType+" "+street+" "+city+" "+state+" "+pincode+" "+images)
    console.log("print image : "+images);

    const prop_statement = `insert into property_details(buildingName, flatNo, bhk, builtUpArea, carpetArea, price, propertyType) VALUES("${buildingName}","${flatNo}","${bhk}","${builtUpArea}","${carpetArea}","${price}","${propertyType}");`

    connection.query(prop_statement,(err,data)=>{
        if(err)
        {
            response.send(utils.createError(err))
        }else{
            propertyId = data.insertId;
            console.log("propertyId : "+propertyId);

            const Properties_statement = `insert into properties(userId, propertyId,isActive) VALUES("${request['userId']}","${propertyId}",1);`
            console.log(Properties_statement);
            connection.query(Properties_statement, (err, data) => {
                if(err)
                {
                    response.send(utils.createError(err))
                }else{
                    console.log("properties_id"+data.insertId);

                    const prop_addr_statement = `insert into address(propertyId, street, city, state, pincode) VALUES("${propertyId}","${street}","${city}","${state}","${pincode}");`
        
                    connection.query(prop_addr_statement,(err,data)=>{
                        if(err)
                        {
                            response.send(utils.createError(err))
                        }else{
                            addressId = data.insertId;
                            console.log("addressId : "+addressId);
                            console.log("before image array ");
                            for (let index = 0; index < images.length; index++) {
                                console.log("inside image array ");
                                const imageName = images[index];
                                console.log(images[index]);
                
                                const prop_img_statement = `insert into images(propertyId, imageName) VALUES("${propertyId}","${imageName}");`
                        
                                connection.query(prop_img_statement,(err,data)=>{
                                    if(err)
                                    {
                                        response.send(utils.createError(err))
                                    }else{
                                        imageId = data.insertId;
                                        console.log("imageId : "+imageId);        
                                    }
                                })
                            }
                        }
                    })
                }
            })
        }
        response.send(utils.createSuccess("Property inserted successfully"));
    })
})

router.post('/:status',(request,response) => {
    console.log(request.params['status'])
    response.send('hello from post change property status properties.js')
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------PUT----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.put('/:propertyId',(request,response) => {
    const {buildingName, flatNo, bhk, builtUpArea, carpetArea, price, propertyType, street, city, state, pincode, images  } = request.body;
    var propertyId;
    propertyId = request.params['propertyId'];
    // console.log(buildingName+" "+bhk+" "+builtUpArea+" "+carpetArea+" "+price+" "+propertyType+" "+street+" "+city+" "+state+" "+pincode+" "+images)
    const prop_statement = `update property_details 
                            SET buildingName = "${buildingName}", flatNo = "${flatNo}", bhk = "${bhk}", builtUpArea = "${builtUpArea}", carpetArea = "${carpetArea}", price = "${price}", propertyType = "${propertyType}" 
                            where propertyId = ${propertyId};`

    connection.query(prop_statement,(err,data)=>{
        if(err)
        {
            response.send(utils.createError(err))
        }else{
            console.log("propertyId : "+data);

                    const prop_addr_statement = `update address  
                                                SET propertyId = "${propertyId}", street = "${street}", city = "${city}", state = "${state}", pincode = "${pincode}" 
                                                where propertyId = ${propertyId};`
        
                    connection.query(prop_addr_statement,(err,data)=>{
                        if(err)
                        {
                            response.send(utils.createError(err))
                        }else{
                            console.log("addressId : "+data);
                        }
                    })
        }
        response.send(utils.createSuccess("Property details are Updated successfully"));
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------DELETE----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.delete('/',(request,response) => {
    response.send('hello from delete properties.js')
})

// --------------------------------------------------------------------------------------------------------

module.exports = router;