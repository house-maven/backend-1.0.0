const express = require('express');
const jwt = require('jsonwebtoken')
const connection = require('../../database_connection')
const utils = require('../../utils');

const crypto = require('crypto-js');
const SendMail = require("../../mailer")
const uuid = require('uuid');
const config = require('../../config');

const fs = require('fs')
const path = require("path");
const { error } = require('console');

const router = express.Router();

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------GET----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.get('/',(request, response) => {
    // response.send('hello from user-server get user.js')
    const statement = `SELECT firstName, lastName, email, phone, type FROM user_info where userId = ${request['userId']};`

    connection.query(statement, (err, data) => {
        response.send(utils.createResult(err, data))
    })
})

// Verifing activation token
router.get('/userAuth/:token',(req,res)=>{
    const token = req.params.token;

    const queryString = `UPDATE user_info SET isActive=1,activationToken="" WHERE activationToken='${token}';`

    connection.query(queryString,(error,data)=>{
        const result = utils.createResult(error,data);

        if(error)
        {
            res.send(utils.createError(error))
        }
        else
        {
            const htmlPath = path.join(__dirname, '/../templates/activation_result.html')
            const body = '' + fs.readFileSync(htmlPath)
            res.header('content-type', 'text/html')
            res.send(body)        
        }
    })
})
// --------------------------------------------------------------------------------------------------------
// -------------------------------------------POST----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.post('/signin',(request,response) => {
    
        const {email, password} = request.body
    
        //ToDo : using SHA256() encrypt the password and then compare with database password
        const statement = `SELECT userId, firstName, lastName, email,type,isActive FROM user_info WHERE email = '${email}' AND password = '${crypto.SHA256(password,config.SECRET_KEY)}'`
        console.log(statement)
        connection.query(statement, (err, data) => {
            if(err){
            response.send(utils.createError(err))
            }else{
                if(data.length == 1){
                    if(data[0]['isActive']==0)
                    {
                        response.send(utils.createError("Account is not activated!")) 
                    }else{
                        // console.log(data[0]['userId'])
                    //To Do create .env for secret key 
                    var token = jwt.sign({ id: data[0]['userId'] }, '0123456789');
                    // console.log(jwt.verify(token, '0123456789'))
                    data[0].token = token ;
                    delete data[0].userId ;
                    response.send(utils.createSuccess(data))
                    }
                }else{
                    response.send(utils.createError('Invalid Credentials!'))
                }
            }
        })        
})

// Signin Route for registering user and sending activation link
router.post('/signup',(request,response) => {
    const {firstName,lastName,email,type,password} = request.body;

    const token = uuid.v4();

    const queryString = `INSERT INTO user_info(firstName,lastName,email,type,password,activationToken)VALUES('${firstName}','${lastName}','${email}','${type}','${crypto.SHA256(password,config.SECRET_KEY)}','${token}')`;

    let body = ""+fs.readFileSync(path.join(__dirname+"/../templates/activation-template.html"));
    
    body = body.replace(/userFullName/,firstName+" "+lastName);

    body = body.replace(/activationLink/,`${config.host}:4000/userAuth/${token}`)

    connection.query(queryString,(error,data)=>{
        const result = utils.createResult(error,data)

        if(error)
        {
            response.send(utils.createError(error))
        }
        else
        {
            SendMail(email,body,"User Account Authentication",(error,info)=>{
                if(error)
                {
                    response.send(utils.createError(error))
                }
                else
                {
                    response.send(utils.createSuccess(info))
                }
            })
        }
    })
})

router.post('/',(request,response) => {
    response.send('hello from user-server post user.js')
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------PUT----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

// TODO : Setup route to "Edit user profile"
router.put('/edit',(request,response) => {
    const userId = request.userId;

    const {firstName,lastName,phone} = request.body;

    const queryString = `UPDATE user_info SET firstName='${firstName}',lastName='${lastName}',phone='${phone}' WHERE userId=${userId}`;
    console.log(queryString)
    connection.query(queryString,(error,data)=>{
        let result = {}

        if(error)
        {
            result.status = "error",
            result.error = error;
        }
        else
        {
            result.status = "success"
            result.data = data
        }

        response.send(result)
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------DELETE----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.delete('/',(request,response) => {
    response.send('hello from user-server delete user.js')
})

// --------------------------------------------------------------------------------------------------------

module.exports = router;