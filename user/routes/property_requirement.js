const express = require('express');
const { response, request } = require('express');
const connection = require('../../database_connection');
const utils = require('../../utils')

const router = express.Router();

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------GET----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.get('/',(request, response) => {
    // response.send('hello from get propertyRequirement.js')
    const queryString = `SELECT pr.id,u.userId,u.firstName,u.lastName,u.email,u.phone,u.type,p.propertyId,p.bhk,p.price,p.propertyType,a.addressId,a.street,a.city,a.state,a.pincode FROM 
    property_requirement pr INNER JOIN user_info u ON pr.userId=u.userId 
    INNER JOIN property_details p ON pr.propertyId=p.propertyId 
    INNER JOIN address a ON a.propertyId=p.propertyId
    where u.userId = ${request['userId']}; `
    console.log(queryString);

    connection.query(queryString, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        }
        else {
            if(data.length)
            {
                // console.log(data);
                response.send(utils.createSuccess(data))
            }else{ 
                response.send(utils.createError("no record found"))
            }
        }
    })
})

router.get('/:id',(request, response) => {
    // response.send('hello from get propertyRequirement.js')

    const{ id } = request.params;

    const queryString = `SELECT pr.id,u.userId,u.firstName,u.lastName,u.email,u.phone,u.type,p.propertyId,p.bhk,p.price,p.propertyType,a.addressId,a.street,a.city,a.state,a.pincode FROM 
    property_requirement pr INNER JOIN user_info u ON pr.userId=u.userId 
    INNER JOIN property_details p ON pr.propertyId=p.propertyId 
    INNER JOIN address a ON a.propertyId=p.propertyId
    where u.userId = ${request['userId']} AND pr.id = ${id}.; `
    console.log(queryString);

    connection.query(queryString, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        }
        else {
            if(data.length)
            {
                // console.log(data);
                response.send(utils.createSuccess(data))
            }else{ 
                response.send(utils.createError("no record found"))
            }
        }
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------POST----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.post('/',(request,response) => {
    const {bhk, price, propertyType, street, city, state, pincode} = request.body;
    var addressId, propertyId;

    const prop_statement = `insert into property_details(bhk, price, propertyType) VALUES("${bhk}","${price}","${propertyType}");`

    connection.query(prop_statement,(err,data)=>{
        if(err)
        {
            response.send(utils.createError(err))
        }else{
            propertyId = data.insertId;
            console.log("propertyId : "+propertyId);

            const Properties_statement = `insert into property_requirement(userId, propertyId) VALUES("${request['userId']}","${propertyId}");`
            console.log(Properties_statement);
            connection.query(Properties_statement, (err, data) => {
                if(err)
                {
                    response.send(utils.createError(err))
                }else{
                    console.log("properties_id"+data.insertId);

                    const prop_addr_statement = `insert into address(propertyId, street, city, state, pincode) VALUES("${propertyId}","${street}","${city}","${state}","${pincode}");`
        
                    connection.query(prop_addr_statement,(err,data)=>{
                        if(err)
                        {
                            response.send(utils.createError(err))
                        }else{
                            addressId = data.insertId;
                            console.log("addressId : "+addressId);
                        }
                    })
                }
            })
        }
        response.send(utils.createSuccess("Property inserted successfully"));
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------PUT----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.put('/:propertyId',(request,response) => {
    const {bhk, price,propertyType, addressId, street, city, state, pincode } = request.body;
    var propertyId;
    propertyId = request.params['propertyId'];

    const prop_statement = `update property_details 
                            SET bhk = ${bhk}, price = ${price},  propertyType = "${propertyType}"
                            where propertyId = ${propertyId};`
    // console.log(prop_statement);
    connection.query(prop_statement,(err,data)=>{
        if(err)
        {
            response.send(utils.createError(err))
        }else{
            // console.log("propertyId : "+data);

                    const prop_addr_statement = `update address  
                                                SET street = "${street}", city = "${city}", state = "${state}", pincode = "${pincode}" 
                                                where addressId = ${addressId};`
        
                    connection.query(prop_addr_statement,(err,data)=>{
                        if(err)
                        {
                            response.send(utils.createError(err))
                        }else{
                            // console.log("addressId : "+data);
                        }
                    })
        }
        response.send(utils.createSuccess("Property requirement are Updated successfully"));
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------DELETE----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.delete('/',(request,response) => {
    response.send('hello from delete propertyRequirement.js')
})

// --------------------------------------------------------------------------------------------------------

module.exports = router;