const express = require('express');
const { response, request } = require('express');
const connection = require("../../database_connection")
const {createResult} = require("../../utils")

const router = express.Router();

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------GET----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

// Getting ratings and feedback
router.get('/',(request, response) => {
    response.send('hello from get ratingAndFeedback.js')
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------POST----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.post('/:propId',(request,response) => {
    const propId = request.params.propId
    const {feedback,ratings} = request.body
    const queryString = `INSERT INTO ratings_and_feedback(propertyId,userId,feedback,ratings)VALUES(${propId},${request.userId},'${feedback}',${ratings})`

    connection.query(queryString,(error,data)=>{
        response.send(createResult(error,data))
    })
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------PUT----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.put('/',(request,response) => {
    response.send('hello from put ratingAndFeedback.js')
})

// --------------------------------------------------------------------------------------------------------
// -------------------------------------------DELETE----------------------------------------------------------
// --------------------------------------------------------------------------------------------------------

router.delete('/',(request,response) => {
    response.send('hello from delete ratingAndFeedback.js')
})

// --------------------------------------------------------------------------------------------------------

module.exports = router;