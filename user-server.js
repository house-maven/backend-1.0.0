//importing required node packages 
const express = require('express');
const { request, Router } = require('express');
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const cors = require("cors")
const morgan = require('morgan');

//importing different routes written in routes folder
const user = require('./user/routes/user')
const userProperties = require('./user/routes/properties')
const userPropertyRequirement = require('./user/routes/property_requirement')
const userRevenue = require('./user/routes/revenue')
const userRatingsAndFeedback = require('./user/routes/ratings_and_feedback');
const { stringify } = require('uuid');

//creating express server
const app = express();

app.use(cors())

app.use(bodyParser.json())
// app.use(express.json())
app.use(morgan('combined'))


// add a middleware for getting the id from token
function getUserId(request, response, next){
    if(request.url == '/signin' || request.url == '/signup' || (request.url).startsWith("/userAuth/") || (request.url).startsWith("/properties/get-image/")){
        next();
    }else{
        try {
            console.log(request.url)
            const token = request.headers['token']
            // console.log(token)
            const data = jwt.verify(token, '0123456789')
            // console.log(data)
            request.userId = data['id']+""

            // console.log(request.userId)
            next();
        } catch (error) {
            response.status(401)
            response.send({status: 'error', error: 'protected api'})
        }
    }
}

//middleware for authenticating user by generating id back from token
 app.use(getUserId)
 
 app.use(express.static('images/'))


//middleware code for calling different routes
app.use('/',user)
app.use('/properties',userProperties)
app.use('/property_requirement',userPropertyRequirement)
app.use('/revenue',userRevenue)
app.use('/ratings_and_feedback',userRatingsAndFeedback)

//user server test request
app.get('/',(request, response) => {
    response.send('hello from user server to postmon');
})


//listening to server on port 3000
app.listen(4000,'0.0.0.0', () => {
    console.log('server started on port 4000')
})